import { getFibonacciNr } from '../scripts/fibonacci';

describe('tests to get n-th fibonacci number', () => {
  test('8-th fibonacci number is 21', async () => {
    // given - nothing to setup
    // when
    const eighthFib = getFibonacciNr(8);
    // then
    const promiseResolved = Promise.resolve({ message: 'hello' });
    expect(promiseResolved).resolves.toEqual({ message: 'hello' });
    const promiseRejected = Promise.reject(new Error('fail'));
    expect(promiseRejected).rejects.toThrow(Error);

    expect(eighthFib).toBe(21);
  });

  test('no fibonacci number if n is negative', () => {
    // given - nothing to setup
    // when
    const noFib = getFibonacciNr(-1);
    // then
    expect(isNaN(noFib)).toBeTruthy();
  });

  test('no fibonacci number if n is not a number', () => {
    // given - nothing to setup
    // when
    const noFib = getFibonacciNr('string');
    // then
    expect(isNaN(noFib)).toBeTruthy();
  });
});
