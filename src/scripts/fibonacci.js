function getFibonacciNr(n) {
  n = parseInt(n);
  if (!n || n < 0) {
    return NaN;
  }
  if (n == 1 || n == 2) {
    return 1;
  }
  return getFibonacciNr(n - 1) + getFibonacciNr(n - 2);
}

export { getFibonacciNr };
